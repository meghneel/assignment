package com.assignment.fbapp;


import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * @class MessageSenderReceiverActivity
 * This class acts as a gateway between the user and GCM. It sends the message the user types 
 * in to GCM.
 * It also caches the sent items into shared preferences so that when it is opened from a 
 * notification, it can show the sent items
 * @author meghneel.gore@gmail.com
 * 
 * Most of the code in this class was copied from the GCM tutorial on Google's website
 */
public class MessageSenderReceiverActivity extends Activity {
	private static final String PREFS_FILE = "saved_messages";
	private static final String MESSAGES_KEY = "messages";
	TextView receivedMessages;
	EditText currentMessage;
	Button sendMessage;
	AtomicInteger msgId = new AtomicInteger();
	GoogleCloudMessaging gcm;
	OnClickListener onSendMessageClick = new OnClickListener() {
		@Override
		public void onClick(View v) {
			new AsyncTask<Void, Void, String>() {
				@Override
				protected String doInBackground(Void... params) {
					String msg = "";
					try {
						Bundle data = new Bundle();
						gcm = GoogleCloudMessaging.getInstance(getApplicationContext());
						data.putString("my_message", currentMessage.getText().toString());
						data.putString("my_action",
								"com.google.android.gcm.demo.app.ECHO_NOW");
						String id = Integer.toString(msgId.incrementAndGet());
						
						gcm.send(FBAppMainActivity.SENDER_ID + "@gcm.googleapis.com", id, data);
						msg = "Sent message: " + currentMessage.getText().toString();
					} catch (IOException ex) {
						msg = "Error :" + ex.getMessage();
					}
					return msg;
				}

				@Override
				protected void onPostExecute(String msg) {
					receivedMessages.append(msg + "\n");
					currentMessage.setText("");
				}
			}.execute(null, null, null);
		}
	};
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.send_receive_message);
		receivedMessages = (TextView) findViewById(R.id.textview_received_messages);
		currentMessage = (EditText)findViewById(R.id.edittext_message);
		sendMessage = (Button)findViewById(R.id.button_sendmessage);
		sendMessage.setOnClickListener(onSendMessageClick);
		loadMessages();
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		saveMessages();
	}
	/**
	 * This could be made to be more sophisticated with each message saved separately.
	 * For now, this loads the whole message list as a \n separated message.
	 */
	private void loadMessages() {
		SharedPreferences prefs = getSharedPreferences(PREFS_FILE , Context.MODE_PRIVATE);
		String messages = prefs.getString(MESSAGES_KEY, null);
		if(messages != null) {
			receivedMessages.setText(messages);
		}
	}


	/**
	 * This could be made to be more sophisticated with each message saved separately.
	 * For now, this saves the whole message list as a \n separated message.
	 */
	private void saveMessages() {
		SharedPreferences prefs = getSharedPreferences(PREFS_FILE , Context.MODE_PRIVATE);
		Editor editor = prefs.edit();
		editor.putString(MESSAGES_KEY, receivedMessages.getText().toString());
		editor.commit();
	}
}
