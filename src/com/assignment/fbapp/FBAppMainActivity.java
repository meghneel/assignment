package com.assignment.fbapp;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.facebook.Request;
import com.facebook.Request.GraphUserCallback;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.model.GraphUser;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import android.support.v7.app.ActionBarActivity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

/**
 * FBAppMainActivity
 * 
 * Main page activity. This has all the setup code for Facebook as well as Google 
 * Cloud Messaging.
 * 
 * @author meghneelgore@gmail.com
 *
 */
public class FBAppMainActivity extends ActionBarActivity implements MainFragment.MainFragmentCallback {

	//TAG for logs
	private static final String TAG = FBAppMainActivity.class.getSimpleName();

	private GoogleCloudMessaging gcm;
	private String regid;

	public static final String EXTRA_MESSAGE = "message";
	public static final String PROPERTY_REG_ID = "registration_id";
	private static final String PROPERTY_APP_VERSION = "appVersion";
	private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

	static String SENDER_ID = "112235737120";

	private SessionCallback statusCallback = new SessionCallback();

	//Class to handle the session callback
	private class SessionCallback implements Session.StatusCallback {

		@Override
		public void call(Session session, SessionState state,
				Exception exception) {
			//If the session state is opened then make a new me request
			//so that we can populate the details on the main page
			if(state == SessionState.OPENED) {
				Request.newMeRequest(session, new GraphUserCallback() {

					@Override
					public void onCompleted(GraphUser user, Response response) {
						mainFragment.populateDetails(user);
						mainFragment.hideLoginProgressBar();
					}
				}).executeAsync();
			}
		}
	}

	//This is required by the Facebook SDK
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
	}

	private MainFragment mainFragment = new MainFragment();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mainFragment.setLoginCallback(this);
		setContentView(R.layout.activity_fbapp_main);

		getSupportFragmentManager().beginTransaction()
		.replace(R.id.container, mainFragment)
		.commit();

		//Register for FB
		Session session = Session.getActiveSession();

		if (session == null) {
			if (savedInstanceState != null) {
				//Restore session from saved instance
				session = Session.restoreSession(this, null, statusCallback, savedInstanceState);
			}
			if (session == null) {
				//Create a new session
				session = new Session(this);
			}
			Session.setActiveSession(session);
			if (session.getState().equals(SessionState.CREATED_TOKEN_LOADED)) {
				session.openForRead(new Session.OpenRequest(this).setCallback(statusCallback));
			}else if(!session.isOpened()) {
				//This will directly show a Facebook dialog.
				//TODO make this login process less ambiguous.
				List<String> permissions = new ArrayList<String>();
				permissions.add("user_education_history");
				permissions.add("user_location");
				permissions.add("basic_info");
				 
				session.openForRead(new Session.OpenRequest(this).setCallback(statusCallback).setPermissions(permissions));
			} 

		} else if(session.getState().equals(SessionState.OPENED)){
			Request.newMeRequest(session, new GraphUserCallback() {

				@Override
				public void onCompleted(GraphUser user, Response response) {
					mainFragment.populateDetails(user);
				}
			}).executeAsync();
		} 

		// Register for GCM
		// Copied code from Google website
		// Check device for Play Services APK. If check succeeds, proceed with
		if (checkPlayServices()) {
			gcm = GoogleCloudMessaging.getInstance(this);
			regid = getRegistrationId(getApplicationContext());

			if (regid.isEmpty()) {
				registerInBackground();
			}
		} else {
			Log.i(TAG, "No valid Google Play Services APK found.");
		}
	}

	/**
	 * Gets the current registration ID for application on GCM service.
	 * <p>
	 * If result is empty, the app needs to register.
	 *
	 * @return registration ID, or empty string if there is no existing
	 *         registration ID.
	 */
	private String getRegistrationId(Context context) {
		final SharedPreferences prefs = getGCMPreferences(context);
		String registrationId = prefs.getString(PROPERTY_REG_ID, "");
		if (registrationId.isEmpty()) {
			Log.i(TAG, "Registration not found.");
			return "";
		}
		// Check if app was updated; if so, it must clear the registration ID
		// since the existing regID is not guaranteed to work with the new
		// app version.
		int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
		int currentVersion = getAppVersion(context);
		if (registeredVersion != currentVersion) {
			Log.i(TAG, "App version changed.");
			return "";
		}
		return registrationId;
	}

	/**
	 * @return Application's version code from the {@code PackageManager}.
	 */
	private static int getAppVersion(Context context) {
		try {
			PackageInfo packageInfo = context.getPackageManager()
					.getPackageInfo(context.getPackageName(), 0);
			return packageInfo.versionCode;
		} catch (NameNotFoundException e) {
			// should never happen
			throw new RuntimeException("Could not get package name: " + e);
		}
	}



	/**
	 * @return Application's {@code SharedPreferences}.
	 */
	private SharedPreferences getGCMPreferences(Context context) {
		// This sample app persists the registration ID in shared preferences, but
		// how you store the regID in your app is up to you.
		return getSharedPreferences(FBAppMainActivity.class.getSimpleName(),
				Context.MODE_PRIVATE);
	}

	/**
	 * Registers the application with GCM servers asynchronously.
	 * <p>
	 * Stores the registration ID and app versionCode in the application's
	 * shared preferences.
	 */
	private void registerInBackground() {
		new AsyncTask<Void, Void, Void>() {
			@Override
			protected Void doInBackground(Void... params) {
				try {
					if (gcm == null) {
						gcm = GoogleCloudMessaging.getInstance(getApplicationContext());
					}
					regid = gcm.register(SENDER_ID);

					// You should send the registration ID to your server over HTTP,
					// so it can use GCM/HTTP or CCS to send messages to your app.
					// The request to your server should be authenticated if your app
					// is using accounts.

					//TODO 
					//I've purposely kept this here so that I know that this is 
					//required -- Meghneel

					// sendRegistrationIdToBackend();

					// For this demo: we don't need to send it because the device
					// will send upstream messages to a server that echo back the
					// message using the 'from' address in the message.

					// Persist the regID - no need to register again.
					storeRegistrationId(getApplicationContext(), regid);
				} catch (IOException ex) {
					//TODO
					// If there is an error, don't just keep trying to register.
					// Require the user to click a button again, or perform
					// exponential back-off.
				}
				return null;
			}
		}.execute();
	}

	/**
	 * Stores the registration ID and app versionCode in the application's
	 * {@code SharedPreferences}.
	 *
	 * @param context application's context.
	 * @param regId registration ID
	 */
	private void storeRegistrationId(Context context, String regId) {
		final SharedPreferences prefs = getGCMPreferences(context);
		int appVersion = getAppVersion(context);
		Log.i(TAG, "Saving regId on app version " + appVersion);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(PROPERTY_REG_ID, regId);
		editor.putInt(PROPERTY_APP_VERSION, appVersion);
		editor.commit();
	}

	/**
	 * Check the device to make sure it has the Google Play Services APK. If
	 * it doesn't, display a dialog that allows users to download the APK from
	 * the Google Play Store or enable it in the device's system settings.
	 */
	private boolean checkPlayServices() {
		int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
		if (resultCode != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
				GooglePlayServicesUtil.getErrorDialog(resultCode, this,
						PLAY_SERVICES_RESOLUTION_REQUEST).show();
			} else {
				Log.i(TAG, "This device is not supported.");
				finish();
			}
			return false;
		}
		return true;
	}

	@Override
	public void onSendMessagesPressed() {
		Intent intent = new Intent(this, MessageSenderReceiverActivity.class);
		startActivity(intent);
	}
	@Override
	public void onShowFriendsPressed() {
		Intent intent = new Intent(this, PickFriendsActivity.class);
		PickFriendsActivity.populateParameters(intent, null, false, true);
		startActivity(intent);
	}

	@Override
	public void onShowMapPressed() {
		Intent intent = new Intent(this, MapActivity.class);
		startActivity(intent);
	}



}
