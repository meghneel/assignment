package com.assignment.fbapp.adapter;

import com.assignment.fbapp.R;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/**
 * EducationArrayAdapter
 * 
 * Adapter for the list on the main page for showing the user's education
 * 
 * @author meghneelgore@gmail.com
 *
 */
public class EducationArrayAdapter extends ArrayAdapter<String> {

	Activity activity;
	String[] list;
	
	public EducationArrayAdapter(Activity context, String[] objects) {
		super(context, R.layout.list_item_education, objects);
		activity = context;
		list = objects;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		if(convertView == null) {
			convertView = inflater.inflate(R.layout.list_item_education, null, true);
		}
		((TextView)convertView).setText(list[position]);
		return convertView;
	}
}
