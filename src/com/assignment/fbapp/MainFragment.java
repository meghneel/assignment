package com.assignment.fbapp;

import org.json.JSONArray;
import org.json.JSONObject;

import com.assignment.fbapp.adapter.EducationArrayAdapter;
import com.facebook.model.GraphLocation;
import com.facebook.model.GraphUser;
import com.facebook.widget.ProfilePictureView;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class MainFragment extends Fragment {

	/**
	 * MainFragmentCallback 
	 * 
	 * Callback interface to communicate with the main activity
	 * 
	 * @author meghneelgore@gmail.com
	 *
	 */
	public interface MainFragmentCallback {
		/**
		 * Called when the show friends button is pressed
		 */
		void onShowFriendsPressed();
		/**
		 * Called when the send messages button is pressed
		 */
		void onSendMessagesPressed();
		/**
		 * Called when the show map button is pressed
		 */
		void onShowMapPressed();
	}

	private MainFragmentCallback loginCallback;


	//The various views on the main page
	private TextView userName;
	private TextView currentCity;
	private ListView education;
	private ProfilePictureView userImage;
	private Button showFriendsButton;
	private Button sendMessages;
	private Button showMap;
	private LinearLayout educationLayout;
	private ProgressBar loginProgress;

	//Fragments are required to have an empty constructor
	public MainFragment() {
	}

	public void setLoginCallback(MainFragmentCallback callback) {
		this.loginCallback = callback;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_fbapp_main, container, false);

		//Obtain references to the different views here
		userName = (TextView) rootView.findViewById(R.id.textview_username);
		userImage = (ProfilePictureView) rootView.findViewById(R.id.imageView_user);
		userImage.setPresetSize(ProfilePictureView.LARGE);
		educationLayout = (LinearLayout) rootView.findViewById(R.id.layout_education);
		currentCity = (TextView) rootView.findViewById(R.id.textview_currentcity);
		education = (ListView) rootView.findViewById(R.id.listview_education);
		showFriendsButton = (Button) rootView.findViewById(R.id.button_show_friends);
		showFriendsButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				loginCallback.onShowFriendsPressed();
			}
		});
		sendMessages = (Button) rootView.findViewById(R.id.button_send_messages);
		sendMessages.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				loginCallback.onSendMessagesPressed();
			}
		});
		showMap = (Button) rootView.findViewById(R.id.button_show_map);
		showMap.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				loginCallback.onShowMapPressed();
			}
		});
		loginProgress = (ProgressBar) rootView.findViewById(R.id.progressbar_login);
		loginProgress.setActivated(true);
		showLoginProgressBar();
		return rootView;
	}


	/**
	 * Makes the progress bar visible and all other elements invisible.
	 * Used when doing session start calls.
	 */
	public void hideLoginProgressBar() {
		userImage.setVisibility(View.VISIBLE);
		userName.setVisibility(View.VISIBLE);
		currentCity.setVisibility(View.VISIBLE);
		educationLayout.setVisibility(View.VISIBLE);
		showFriendsButton.setVisibility(View.VISIBLE);
		sendMessages.setVisibility(View.VISIBLE);
		showMap.setVisibility(View.VISIBLE);

		loginProgress.setVisibility(View.INVISIBLE);
	}
	/**
	 * Makes the progress bar invisible and all other elements visible.
	 * Used when doing session start calls.
	 */
	public void showLoginProgressBar() {
		userImage.setVisibility(View.INVISIBLE);
		userName.setVisibility(View.INVISIBLE);
		currentCity.setVisibility(View.INVISIBLE);
		educationLayout.setVisibility(View.INVISIBLE);
		showFriendsButton.setVisibility(View.INVISIBLE);
		sendMessages.setVisibility(View.INVISIBLE);
		showMap.setVisibility(View.INVISIBLE);

		loginProgress.setVisibility(View.VISIBLE);
	}

	/**
	 * Populates the details of the main page including the user's picture, location
	 * and education.
	 * Must call this on UI thread.
	 * @param user The GraphUser object of the logged in user
	 */
	public void populateDetails(GraphUser user) {
		if(user == null) return;
		String name = user.getName();
		String userId = user.getId();
		GraphLocation location = user.getLocation();
		String curCity = (String) location.getProperty("name");
		final JSONArray edu = (JSONArray) user.getProperty("education");
		//Id and name don't need null checks since they're part of the basic elements
		userImage.setProfileId(userId);
		userName.setText(name);

		if(currentCity != null) {
			currentCity.setText(curCity);
		} else {
			currentCity.setText(getActivity().getString(R.string.current_city_unknown));
		}

		new Thread() {
			String[] educationList = null;
			@Override
			public void run() {
				if(edu != null) {
				educationList = new String[edu.length()];
				for(int i = 0; i < edu.length(); i++) {
						JSONObject school = (JSONObject) edu.opt(i);
						JSONObject schoolNameJSON = (JSONObject) school.opt("school");
						String schoolName = schoolNameJSON.optString("name");
						educationList[i] = schoolName;
					} 
				} else {
					educationList = new String[1];
					educationList[0] = getActivity().getString(R.string.no_education_information);
				}

				getActivity().runOnUiThread(new Runnable() {

					@Override
					public void run() {
						EducationArrayAdapter educationAdapter = new EducationArrayAdapter(getActivity(), educationList);
						education.setAdapter(educationAdapter);
						educationAdapter.notifyDataSetChanged();
						hideLoginProgressBar();
					}

				});

			}
		}.start();
	}
}
