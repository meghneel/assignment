package com.assignment.fbapp;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;

import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

public class MapActivity extends FragmentActivity {

	private GoogleMap mMap;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_map);
	}

	@Override
	protected void onResume() {

		super.onResume();
		if (mMap == null) {
			mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
					.getMap();
			if (mMap != null) {
				//Set the location overlay
				mMap.setMyLocationEnabled(true);

				//Get the user's location
				LocationManager locationManager = (LocationManager)getSystemService(LOCATION_SERVICE);
				String provider = locationManager.getBestProvider(new Criteria(), true);
				Location location = locationManager.getLastKnownLocation(provider);

				if (location != null){
					setLocation(location);
				}
			}	
		}
	}

	public void setLocation(Location location) {
		//Get the latitude and longitude
		LatLng latlng=new LatLng(location.getLatitude(),location.getLongitude());
		//Move the camera in an animated fashion
		mMap.moveCamera(CameraUpdateFactory.newLatLng(latlng));
		mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latlng, 15.0f));
	}

}
